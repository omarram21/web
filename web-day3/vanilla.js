
function validateForm() {
  const form = document.getElementById("sponsor-registration-form");
  const fName = form.elements["f-name"].value;
  const lName = form.elements["l-name"].value;
  const email = form.elements["email"].value;
  const facebool = form.elements["company-facebook-name"].value;

  if (fName.trim() === "") {
    alert("First name is required");
    return false;
  }

  if (lName.trim() === "") {
    alert("Last name is required");
    return false;
  }
  const regex = /^[a-zA-Z]+$/;

  if (!regex.test(fName.value)) {
    alert('First name should only contain letters');
  }
  
  if (!regex.test(lName.value)) {
    alert('Last name should only contain letters');
  }

  if (email.trim() === "") {
    alert("Email is required");
    return false;
  }

  if (!/\S+@\S+\.\S+/.test(email)) {
    alert("Invalid email format");
    return false;
  }
  
  if(!facebool.startsWith('https://facebook.com'))
    {
    alert("the facebook url should start with https://facebook.com ");
      return false ; 
    }
  // Add more validation rules as needed

  return true;
}


const form = document.getElementById("sponsor-registration-form");
form.addEventListener("submit", function(event) {
  event.preventDefault();
  if (validateForm()) {
    // Submit the form
    form.submit();
  }
});

$( function() {
  $( "#menu" ).menu();
});
$(function() {
  $("#menu").menu({
    // Add any additional settings here
  });

  // Handle click events on menu items
  $("#menu li").on("click", function() {
    // Handle the click event
  });
});


$(document).ready(function(){
  // Initialize the Slick library on the image carousel container
  $('.image-carousel').slick({
    dots: true,
    infinite: true,
    centerMode: true,
    centerPadding: '60px',
    speed: 300,
    slidesToShow:2,
    slidesToScroll: 1,
    
  });
});
